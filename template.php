<?php

function friendselectric_wrap_content($text) {
  $text = preg_replace('!<pre>!i', '<div class="pre"><pre>', $text);
  $text = preg_replace('!</pre>!i', '</pre></div>', $text);
  return $text;
}
function friendselectric_wrap_links($link, $n) {
  $classes = array("lw1", "lw2");
  $before = $after = "";
  foreach ($classes as $c) {
    $before .= '<span class="'. $c .'">';
    $after .= '</span>';
  }
  $link = preg_replace('!<a[^>]*>!i', '\0'. $before, $link);
  $link = preg_replace('!</a[^>]*>!i', $after . '\0', $link);
  return $link;
}

function friendselectric_menu_item_link($link) {
  //  $link = l('<span class="lw1">'. check_plain($link['title']) .'</span>', $link['href'], array('html' => TRUE));
    if (empty($link['options'])) {
    $link['options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="lw1">' . check_plain($link['title']) . '</span>';
    $link['options']['html'] = TRUE;
  }

  if (empty($link['type'])) {
    $true = TRUE;
  }

  return l($link['title'], $link['href'], $link['options']);
  
  return $link;
}


function phptemplate_menu_primary_links($primary_links) {
  foreach ($primary_links as $link) {
    $links[] = l('<span class="lw3">'. check_plain($link['title']) .'</span>', $link['href'], array('html' => TRUE));
  }
  
  return $links;
}

function friendselectric_comment_thread_collapsed($comment) {
  if ($comment->depth) {
    $output  = '<div style="padding-left:'. ($comment->depth * 25) ."px;\">\n";
    $output .= theme('comment_view', $comment, '', 0);
    $output .= "</div>\n";
  }
  else {
    $output .= theme('comment_view', $comment, '', 0);    
  }
  return $output;
}

function friendselectric_comment_thread_expanded($comment) {
  $output = '';
  if ($comment->depth) {
    $output .= '<div style="padding-left:'. ($comment->depth * 25) ."px;\">\n";
  }

  $output .= theme('comment_view', $comment, module_invoke_all('link', 'comment', $comment, 0));

  if ($comment->depth) {
    $output .= "</div>\n";
  }
  return $output;
}


/**
 * Override or insert PHPTemplate variables into the page templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("page" in this case.)
 */
function friendselectric_preprocess_page(&$vars, $hook) {
  drupal_set_message("<strong>Have knowledge in CSS?</strong> Friendselectric needs to be fixed for IE6 (menu bg's), IE7 (box width) and content width across all browsers. If you could lend a hand, please submit a fixed version (patch or zip) via the this <a href=\"http://drupal.org/project/color_soc08\">project page</a> or on this <a href=\"http://drupal.org/node/170948\">issue page</a>. You will get credit and be a hero!", 'status', FALSE);
}

/**
 * Return the path to the main nista theme directory.
 */
function path_to_friendselectrictheme() {
  static $theme_path;
  if (!isset($theme_path)) {
    global $theme;
    if ($theme == 'nista') {
      $theme_path = path_to_theme();
    }
    else {
      $theme_path = drupal_get_path('theme', 'friendselectric');
    }
  }
  return $theme_path;
}

/**
 *  Implements hook_color() for theme.
 *  Color replacement method(s): 'shift' (base, text, link)
 *                                'tag' custom tag replacement
 */
function friendselectric_color() {
	/**
	 *  $color = array() (Required)
	 *  For essential scheme information.
	 *
	 */
	$color = array(
	  /**
	   *  'replacement methods'
	   *  Declare replacement replacement methods in the 'replacement methods' array, 
	   *  in the order you want them to work.
	   *    They are comprised different algorithms to change your colors.
	   *  replacement methods like 'shift' can accept different options and behave differently.
	   *  To change the order of the replacement methods color replacement, put them in order.
	   */
	  'replacement methods' => array(
	    /**
	     *  'shift' method
	     *  Requires 'base', and/or'link' and/or 'text'
	     */
	    'shift' => array('base', 'text', 'link'),
	    /**
	     *  'tag' method
	     *  Replaces custom tags in stylesheet based on fieldname.
	     */
	    'tag' => TRUE,
	  ),

	  /**
	   *  'fields' (Required)
	   *  The machine-readable names of color fields. The name will help you
	   *  and your theme users identify colors.
	   */
	  'fields' => array('base', 'link', 'top', 'bottom', 'text', 'main-link', 'menu-link'),

	  /**
	   *  'premade schemes' (Required)
	   *  A list of pre-made schemes that will be available for install by default.
	   *  The scheme name, human-readable, is followed by hex values separated by
	   *  commas (no spaces). The colors are to be ordered respectable to 'fields'.
	   */
	  'premade schemes' => array(
	    'Aquamarine' => array(
                           'base' => '#55c0e2', 'link' => '#000000', 'top' => '#085360', 'bottom' => '#007e94', 'text' => '#696969', 'main-link' => '#000000', 'menu-link' => '#085360'),
	    'Ash' => array('base' =>  '#464849' ,'link' => '#2f416f', 'top' => '#2a2b2d', 'bottom' => '#5d6779', 'text' => '#494949', 'main-link' => '#262626', 'menu-link' => '#2a2b2d'),
	    'Belgian Chocolate' => array('base' => '#d5b048' ,'link' => '#6c420e', 'top' => '#331900', 'bottom' => '#971702,', 'text' => '#494949', 'main-link' => '#6c420e', 'menu-link' => '#331900'),
	    'Blue Lagoon' => array('base' => '#0072b9' ,'link' => '#027ac6', 'top' => '#2385c2', 'bottom' => '#5ab5ee', 'text' => '#494949', 'main-link' => '#027ac6', 'menu-link' => '#2385c2'),
	    'Bluemarine' => array('base' => '#3f3f3f' ,'link' => '#336699', 'top' => '#6598cb', 'bottom' => '#3a71ab', 'text' => '#000000', 'main-link' => '#336699', 'menu-link' => '#6598cb'),
	    'Citrus Blast' => array('base' => '#e25912','link' => '#6f5c06', 'top' => '#f46715', 'bottom' => '#9a5707', 'text' => '#0b2645', 'main-link' => '#6f5c06', 'menu-link' => '#df8920'),
	    'Cold Day' => array('base' => '#0f005c','link' => '#434f8c', 'top' => '#4d91ff', 'bottom' => '#1a1575', 'text' => '#000000', 'main-link' => '#434f8c', 'menu-link' => '#4d91ff'),
	    'Electric' => array('base' => '#D9EEF1','link' => '#255FB7', 'top' => '#78aac7', 'bottom' => '#375d8a', 'text' => '#0b2645', 'main-link' => '#E60A53', 'menu-link' => '#78aac7'),
	    'Greenbeam' => array('base' => '#b7b075','link' => '#0c7a00', 'top' => '#03961e', 'bottom' => '#5fae00', 'text' => '#2f2f2f', 'main-link' => '#0c7a00', 'menu-link' => '#024b0f'),
	    'Majestic' => array('base' => '#5c00b9','link' => '#027ac6', 'top' => '#f028ab', 'bottom' => '#5f009e', 'text' => '#494949', 'main-link' => '#027ac6', 'menu-link' => '#f028ab'),
	    'Mediterrano' => array('base' => '#ffe23d','link' => '#a9290a', 'top' => '#fc6d1d', 'bottom' => '#a30f42', 'text' => '#494949', 'main-link' => '#a9290a', 'menu-link' => '#fc6d1d'),
	    'Nocturnal' => array('base' => '#5b5fa9','link' => '#5b5faa', 'top' => '#0a2352', 'bottom' => '#9fa8d5', 'text' => '#494949', 'main-link' => '#5b5faa', 'menu-link' => '#0a2352'),
	    'Olivia' => array('base' => '#7db323','link' => '#6a9915', 'top' => '#b5d52a', 'bottom' => '#7db323', 'text' => '#191a19', 'main-link' => '#6a9915', 'menu-link' => '#b5d52a'),
	    'Pink Plastic' => array('base' => '#12020b','link' => '#1b1a13', 'top' => '#f391c6', 'bottom' => '#f41063', 'text' => '#898080', 'main-link' => '#1b1a13', 'menu-link' => '#f391c6'),
	    'Shiny Tomato' => array('base' => '#cb1025','link' => '#c70000', 'top' => '#a1443a', 'bottom' => '#f21107', 'text' => '#515d52', 'main-link' => '#c70000', 'menu-link' => '#a1443a'),
	    'Teal Top' => array('base' => '#18583d','link' => '#1b5f42', 'top' => '#34775a', 'bottom' => '#52bf90', 'text' => '#2d2d2d', 'main-link' => '#1b5f42', 'menu-link' => '#34775a'),
	  ),

	  /**
	   *  'default scheme' (Required)
	   *  A scheme name from one in your 'premade schemes' list.
	   */
	  'default scheme' => 'Electric',

	  /**
	   *  'reference scheme' (Required)
	   *  A scheme name from one in your 'premade schemes' list.
	   *  On schemes which use the 'base', 'link' or 'text' flag, always mention this.
	   *  Other schemes will be shifted against the fields of the reference one.
	   */
	  'reference scheme' => 'Electric',

	  /**
	   *  'stylesheets' (Required)
	   *  Path to your CSS file relevant to the root of your theme directory.
	   *  Can mention an array of stylesheets.
	   */
	  'stylesheets' => array(
	    'color/friendselectric.css',
	  ),

	  /**
	   *  'blend target' (Required)
	   *  Used as a target color for image and shifting colors to and fro.
	   *  Usually #ffffff, #000000, or #666666 can do.
	   */
	  'blend target' => '#ffffff',
	);

	/**
	 *  $img['images'][imgname] (Required)
	 *  Transparent images you wish to fill the BG's of.
	 *  And/or slice. List as many as you want, keep the imgname unique.
	 */
	$color['images']['header'] = array(
	  /**
	   *  'file' (Required)
	   *  Path to your source image in terms of theme's root.
	   */
	  'file' => 'images/header-mid-blank.png',

	  /**
	   *  'fill' (Optional)
	   *  Solid: x-coordinate, y-coordinate, width, height, fill color
	   *  Gradient: x-coordinate, y-coordinate, width, height, top color, bottom color
	   *  You can list as many solid/gradient fills as you want
	   */
	  'fill' => array(
	    // x coord start, y coord start, width, height, top fieldname, bottom fieldname
	    array('type' => 'y-gradient',
                  'x' => 0,
                  'y' => 0,
                  'width' => 2,
                  'height' => 114,
                  'colors' => array('top', 'bottom'),
                  ),
	  ),

	  /**
	   *  'slices' (Required)
	   *  Slice your image up to file destination.
	   *  If your slice output file matches a CSS image BG it will be replaced
	   *  automatically for you per scheme. :)
	   *  file-path according theme root  => x-coordinate, y-coordinate, width, height
	   */
	  'slices' => array(
	    'images/header-mid.png' => array(
                                             'x' => 0,
                                             'y' => 0,
                                             'width' => 2,
                                             'height' => 114
                                             ),
	  ),
	);

	$color['images']['nav-left'] = array(
	  'file' => 'images/nav-left-blank.png',

	  /**
	   *  'fill' (Optional)
	   *  Solid: x-coordinate, y-coordinate, width, height, fill color
	   *  Gradient: x-coordinate, y-coordinate, width, height, top color, bottom color
	   *  You can list as many solid/gradient fills as you want
	   */
	  'fill' => array(
	    array('type' => 'solid',
                  'x' => 0,
                  'y' => 1,
                  'width' => 1,
                  'height' => 16,
                  'colors' => array('top'),
                  ),
	    array('type' => 'solid',
                  'x' => 1,
                  'y' => 0,
                  'width' => 4,
                  'height' => 19,
                  'colors' => array('top'),
                  ),
	  ),

	  /**
	   *  'transparent color' (Optional)
	   *  A hex color which you want to remain tranparent.
	   *  Currently this is used only for keeping blank areas
	   *  of transparent PNG's clear.
	   */
	  'transparent color' => '#000000',

	  'slices' => array(
	    'images/nav-left.png' => array(
                                           'x' => 0,
                                           'y' => 0,
                                           'width' => 5,
                                           'height' => 19
                                           ),
	  ),
	);

	$color['images']['nav-right'] = array(
	  'file' => 'images/nav-right-blank.png',
	  'fill' => array(
	    array('type' => 'solid',
                  'x' => 0,
                  'y' => 0,
                  'width' => 3,
                  'height' => 19,
                  'colors' => array('top'),
                 ),
	    array('type' => 'solid',
                  'x' => 4,
                  'y' => 1,
                  'width' => 1,
                  'height' => 16,
                  'colors' => array('op'),
                 ),
	  ),
	  'transparent color' => '#000000',
	  'slices' => array(
	    'images/nav-right.png'  => array('x' => 0,
                                             'y' => 0,
                                             'width' => 5,
                                             'height' => 19
                                             ),
	  ),
	);

	$color['images']['bg-left'] = array(
	  'file' => 'images/bg-left-blank.png',
	  'fill' => array(
	    array('type' => 'solid',
                  'x' => 0,
                  'y' => 0,
                  'width' => 33,
                  'height' => 22,
                  'colors' => array('base'),
                 ),
	  ),
	  'transparent color' => '#000000',
	  'slices' => array(
	    'images/bg-left.png'  => array(
                                           'x' => 0,
                                           'y' => 0,
                                           'width' => 33,
                                           'height' => 20
                                           ),
	  ),
	);

	$color['images']['bg-right'] = array(
	  'file' => 'images/bg-right-blank.png',
	  'fill' => array(
	    array('type' => 'solid',
                  'x' => 0,
                  'y' => 0,
                  'width' => 33,
                  'height' => 20,
                  'colors' => array('base'),
                 ),
	  ),
	  'transparent color' => '#000000',
	  'slices' => array(
	    'images/bg-right.png' => array('x' => 0,
                                           'y' => 0,
                                           'width' => 33,
                                           'height' => 20
                                           ),
	  ),
	);

	$color['images']['bg-bottom-left-thin'] = array(
	  'file' => 'images/bottom-left-thin-blank.png',
	  'fill' => array(
	    array('type' => 'solid',
                  'x' => 0,
                  'y' => 0,
                  'width' => 35,
                  'height' => 25,
                  'colors' => array('base'),
                 ),
	  ),
	  'transparent color' => '#000000',
	  'slices' => array(
	    'images/bottom-left-thin.png' => array('x' => 0,
                                                   'y' => 0,
                                                   'height' => 35,
                                                   'width' => 25,
                                                   ),
	  ),
	);

	$color['images']['bg-bottom-right-thin'] = array(
	  'file' => 'images/bottom-right-thin-blank.png',
	  'fill' => array(
	    array('type' => 'solid',
                  'x' => 0,
                  'y' => 0,
                  'width' => 35,
                  'height' => 25,
                  'colors' => array('base')
                  ),
	  ),
	  'transparent color' => '#000000',
	  'slices' => array(
	    'images/bottom-right-thin.png'  => array(
                                                     'x' => 0,
                                                     'y' => 0,
                                                     'width' => 35,
                                                     'height' => 25
                                                     ),
	  ),

	);

	$color['images']['bg-bottom-left'] = array(
	  'file' => 'images/bottom-left-blank.png',
	  'fill' => array(
	    array('type' => 'solid',
                  'x' => 0,
                  'y' => 0,
                  'width' => 55,
                  'height' => 35,
                  'colors' => array('base'),
                  ),
	  ),
	  'transparent color' => '#000000',
	  'slices' => array(
	    'images/bottom-left.png'  => array('x' => 0,
                                               'y' => 0,
                                               'width' => 234,
                                               'height' => 35
                                               ),
	  ),
	);


	$color['images']['bg-bottom-right'] = array(
	  'file' => 'images/bottom-right-blank.png',
	  'fill' => array(
	    array('type' => 'solid',
                  'x' => 190,
                  'y' => 0,
                  'width' => 55,
                  'height' => 35,
                  'colors' => array('base'),
                 ),
	  ),
	  'transparent color' => '#000000',
	  'slices' => array(
	    'images/bottom-right.png' => array('x' => 0,
                                               'y' => 0,
                                               'width' => 234,
                                               'height' => 35
                                               ),
	  ),
	);
	
	return $color;
}